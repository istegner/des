﻿using System;
using System.IO;

namespace DES_Server
{
    class Debug
    {
        public static void Log(string message)
        {
            Console.WriteLine(message);
        }

        public static void LogWarning(string message)
        {
            LogError(message);
        }

        public static void LogError(string message)
        {
            Log(message);
            DateTime localDT = DateTime.Now;
            string toWrite = localDT.ToString() + "\n" + message + "\n\n\n";

            string path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "!outputLog.txt");
            if (!File.Exists(path))
            {
                using (StreamWriter sw = File.CreateText(path))
                {
                    sw.WriteLine(toWrite);
                }
            }
            else
            {
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine(toWrite);
                }
            }
        }
    }
}
