﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using SFML;
using SFML.Graphics;
using SFML.Window;
using SFML.System;

namespace DES_Server
{
    class Program
    {
        public bool shutdown = false;
        private const int port = 1379; //The port
        List<Client> clients = new List<Client>(); //A list of connected clients

        UdpClient listener = new UdpClient(port); //What to listen on for packets

        const int screenWidth = 1920;
        const int screenHeight = 1080;

        const int potMaxX = 900;
        const int potMaxY = 900;

        byte[] screenBuffer;

        //Start the program
        static int Main()
        {
            Program p = new Program();
            p.MainLoop();
            return 0;
        }

        int MainLoop()
        {
            Debug.Log("Starting...");

            screenBuffer = new byte[screenWidth * screenHeight * 4];
            ContextSettings contextSettings = new ContextSettings();
            contextSettings.DepthBits = 32;

            // Create the main window
            RenderWindow window = new RenderWindow(new VideoMode((uint)screenWidth, (uint)screenHeight), "SFML graphics with OpenGL", Styles.Default, contextSettings);
            window.SetVerticalSyncEnabled(false);
            window.Closed += new EventHandler(OnClosed);

            Texture screenTex = new Texture((uint)screenWidth, (uint)screenHeight);
            Sprite screenSprite = new Sprite(screenTex);

            clear(255, 255, 255);

            IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, port);
            byte[] recieveByteArray;

            try
            {
                //While the server is running
                while (!shutdown && window.IsOpen)
                {
                    //Debug.Log("Waiting for packet");
                    if (listener.Available > 0)
                    {
                        recieveByteArray = listener.Receive(ref endPoint);
                                                                                          //Check if packet recieved from already connected client
                        bool clientFound = ClientConnected(endPoint.Address);
                        if (clientFound)
                        {
                            Client currClient = GetClient(endPoint.Address);
                            //Client is connected so do the stuff we want to do with it
                            switch (recieveByteArray[0])
                            {
                                //We recieved an update packet
                                case (byte)Packet.packetTypes.UPDATE:
                                    {
                                        int packetNum = BitConverter.ToUInt16(recieveByteArray, 1);
                                        if (packetNum > currClient.lastPacket)
                                        {

                                            //The position of the clients pots
                                            float x = BitConverter.ToUInt16(recieveByteArray, 5);
                                            float y = BitConverter.ToUInt16(recieveByteArray, 7);

                                            x = (x / potMaxX) * screenWidth;
                                            y = (y / potMaxY) * screenHeight;

                                            DateTime now = DateTime.Now;
                                            Debug.Log(now.ToString() + " " + currClient.iPAddress.ToString() + " " + " x:" + (int)x + " y:" + (int)y);
                                            line(currClient.currXPos, currClient.currYPos, (int)x, (int)y, currClient.r, currClient.g, currClient.b);
                                            currClient.currXPos = (int)x;
                                            currClient.currYPos = (int)y;

                                            currClient.lastPacket = packetNum;
                                        }

                                        currClient.timeLastPacketRecieved = DateTime.Now;

                                        break;
                                    }
                                case (byte)Packet.packetTypes.ANNOUNCE:
                                    {
                                        Acknowledge(endPoint, recieveByteArray);
                                        break;
                                    }
                            }
                        }
                        //Client not in the list
                        else
                        {
                            switch (recieveByteArray[0])
                            {
                                //Announce packet recieved
                                case (byte)Packet.packetTypes.ANNOUNCE:
                                    {
                                        Acknowledge(endPoint, recieveByteArray);
                                        break;
                                    }
                                default:
                                    {
                                        Debug.Log("Packet recieved was not from connected client and was not an announce packet");
                                        break;
                                    }
                            }
                        }
                    }
                    screenTex.Update(screenBuffer);
                    window.DispatchEvents();
                    window.Clear();
                    window.Draw(screenSprite);
                    window.Display();

                    if (clients.Count > 0) CheckTimedOutClients();

                    if (Keyboard.IsKeyPressed(Keyboard.Key.Space))
                    {
                        clear(255, 255, 255);
                    }
                    if (Keyboard.IsKeyPressed(Keyboard.Key.Escape))
                    {
                        Shutdown();
                    }
                }
            }
            //Something went wrong
            catch (Exception e)
            {
                Debug.LogError("Error: " + e.ToString());
            }

            //Shutdown the server
            if (!shutdown) Shutdown();
            return 0;
        }

        void Acknowledge(IPEndPoint _endPoint, byte[] _recieveByteArray)
        {
            //Server full, send server full packet back
            if (clients.Count >= 4)
            {
                byte[] sendBuffer = ToByteArray(new Pkt_Error_ServerFull());
                Send(sendBuffer, _endPoint);
                Debug.Log("Client failed to connect: Server full");
            }
            //Server not full, add it
            else
            {
                float x = BitConverter.ToUInt16(_recieveByteArray, 11);
                float y = BitConverter.ToUInt16(_recieveByteArray, 13);

                x = (x / potMaxX) * screenWidth;
                y = (y / potMaxY) * screenHeight;

                //If client is already in the list, dont re-add it
                if (!ClientConnected(_endPoint.Address))
                {
                    Client client = new Client()
                    {
                        iPAddress = _endPoint.Address,
                        lastPacket = 0,
                        timeLastPacketRecieved = DateTime.Now,
                        currXPos = (int)x,
                        currYPos = (int)y
                    };
                    //Add it to the list
                    clients.Add(client);
                    switch (_recieveByteArray[10])
                    {
                        case 0:
                            {
                                client.r = 255;
                                client.g = 0;
                                client.b = 0;
                                break;
                            }
                        case 1:
                            {
                                client.r = 0;
                                client.g = 255;
                                client.b = 0;
                                break;
                            }
                        case 2:
                            {
                                client.r = 0;
                                client.g = 0;
                                client.b = 255;
                                break;
                            }
                        case 3:
                            {
                                client.r = 0;
                                client.g = 255;
                                client.b = 255;
                                break;
                            }
                    }
                }
                //Send back an acknowledge packet
                byte[] sendBuffer = ToByteArray(new Pkt_Ack());
                Send(sendBuffer, _endPoint);
                Debug.Log("Client connected: " + _endPoint.Address);
                Debug.Log("Client start pos: " + (int)x + "," + (int)y);
                //TODO: Set the start pos of the cursor in graphics stuff
                clients[clients.Count - 1].currXPos = (int)x;
                clients[clients.Count - 1].currYPos = (int)y;
            }
        }

        bool ClientConnected(IPAddress toCheck)
        {
            for (int i = 0; i < clients.Count; i++)
            {
                if (toCheck.ToString() == clients[i].iPAddress.ToString())
                {
                    return true;
                }
            }
            return false;
        }

        Client GetClient(IPAddress address)
        {
            for (int i = 0; i < clients.Count; i++)
            {
                if (address.ToString() == clients[i].iPAddress.ToString())
                {
                    return clients[i];
                }
            }
            return null;
        }

        byte[] ToByteArray(Packet obj)
        {
            return obj.ToSend();
        }

        void Send(byte[] msg, IPEndPoint endPoint)
        {
            listener.Send(msg, msg.Length, endPoint);
        }

        /// <summary>
        /// Send the shutdown packet to all connect clients and disallow connection of new clients
        /// </summary>
        void Shutdown()
        {
            shutdown = true;
            if (clients.Count > 0)
            {
                byte[] sendBuffer = ToByteArray(new Pkt_Shutdown());
                foreach (Client c in clients)
                {
                    //UdpClient udp = new UdpClient(port);
                    IPEndPoint endPoint = new IPEndPoint(c.iPAddress, port);
                    listener.Send(sendBuffer, sendBuffer.Length, endPoint);
                }
            }

            listener.Close();
        }

        void CheckTimedOutClients()
        {
            for (int i = 0; i < clients.Count; ++i)
            {
                double timeDiff = (DateTime.Now - clients[i].timeLastPacketRecieved).TotalSeconds;
                if (timeDiff >= 10)
                {
                    Debug.LogWarning("Client timed out: " + clients[i].iPAddress.ToString());
                    clients.RemoveAt(i);
                    --i;
                }
            }
        }

        static void OnClosed(object sender, EventArgs e)
        {
            RenderWindow window = (RenderWindow)sender;
            window.Close();
        }

        void setPixel(int x, int y, byte r, byte g, byte b)
        {
            if (x >= 0 && x < screenWidth && y >= 0 && y < screenHeight)
            {
                screenBuffer[x * 4 + y * screenWidth * 4 + 0] = r;
                screenBuffer[x * 4 + y * screenWidth * 4 + 1] = g;
                screenBuffer[x * 4 + y * screenWidth * 4 + 2] = b;
                screenBuffer[x * 4 + y * screenWidth * 4 + 3] = 0xff;
            }
        }
        void clear(byte r, byte g, byte b)
        {
            for (int y = 0; y < screenHeight; ++y)
            {
                for (int x = 0; x < screenWidth; ++x)
                {
                    screenBuffer[x * 4 + y * screenWidth * 4 + 0] = r;
                    screenBuffer[x * 4 + y * screenWidth * 4 + 1] = g;
                    screenBuffer[x * 4 + y * screenWidth * 4 + 2] = b;
                    screenBuffer[x * 4 + y * screenWidth * 4 + 3] = 0xff;
                }
            }

        }
        void line(int x0, int y0, int x1, int y1, byte r, byte g, byte b)
        {
            int dx = Math.Abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
            int dy = Math.Abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
            int err = (dx > dy ? dx : -dy) / 2, e2;
            for (; ; )
            {
                setPixel(x0, y0, r, g, b);
                if (x0 == x1 && y0 == y1) break;
                e2 = err;
                if (e2 > -dx) { err -= dy; x0 += sx; }
                if (e2 < dy) { err += dx; y0 += sy; }
            }
        }
    }
}
