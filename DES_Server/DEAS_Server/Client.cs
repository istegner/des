﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DES_Server
{
    class Client
    {
        public byte r;
        public byte g;
        public byte b;
        public IPAddress iPAddress;
        public int lastPacket;
        public DateTime timeLastPacketRecieved;
        public int currXPos;
        public int currYPos;
    }
}
