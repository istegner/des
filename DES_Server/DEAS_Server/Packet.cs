﻿using System.Runtime.InteropServices;
using System;

namespace DES_Server
{
    /// <summary>
    /// Base packet class
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 1)]
    [System.Serializable]
    public abstract class Packet
    {
        [FieldOffset(0)] public byte packetType;
        [FieldOffset(1)] public int packetNum;

        public enum packetTypes
        {
            ANNOUNCE,
            ACKNOWLEDGE,
            UPDATE,
            SHUTDOWN,
            SERVERFULL
        }

        public abstract byte[] ToSend();
    }

    /// <summary>
    /// Packet type 0;
    /// Sent by the client to try to connect to the server
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 1)]
    [System.Serializable]
    public class Pkt_Announce : Packet
    {
        [FieldOffset(5)] public byte[] msg = new byte[5] { (byte)'H', (byte)'e', (byte)'l', (byte)'l', (byte)'o' };
        [FieldOffset(10)] public byte clientColour;
        [FieldOffset(11)] public short xStart;
        [FieldOffset(13)] public short yStart;

        public Pkt_Announce()
        {
            packetType = (byte)packetTypes.ANNOUNCE;
        }
        public override byte[] ToSend()
        {
            byte[] temp = new byte[10];
            BitConverter.GetBytes(packetType).CopyTo(temp, 0);
            BitConverter.GetBytes(packetNum).CopyTo(temp, 1);
            msg.CopyTo(temp, 5);
            BitConverter.GetBytes(clientColour).CopyTo(temp, 10);
            BitConverter.GetBytes(xStart).CopyTo(temp, 11);
            BitConverter.GetBytes(xStart).CopyTo(temp, 13);
            return temp;
        }
    }

    /// <summary>
    /// Packet type 1;
    /// Sent to the client to acknowledge it has connected
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 1)]
    [System.Serializable]
    public class Pkt_Ack : Packet
    {
        [FieldOffset(5)] public byte r;
        [FieldOffset(6)] public byte g;
        [FieldOffset(7)] public byte b;
        //[FieldOffset(8)] public short axisMaxVal;

        public Pkt_Ack()
        {
            packetType = (byte)packetTypes.ACKNOWLEDGE;
        }

        public override byte[] ToSend()
        {
            byte[] temp = new byte[10];
            BitConverter.GetBytes(packetType).CopyTo(temp, 0);
            BitConverter.GetBytes(packetNum).CopyTo(temp, 1);
            BitConverter.GetBytes(r).CopyTo(temp, 5);
            BitConverter.GetBytes(g).CopyTo(temp, 6);
            BitConverter.GetBytes(b).CopyTo(temp, 7);
            //BitConverter.GetBytes(axisMaxVal).CopyTo(temp, 8);
            return temp;
        }
    }

    /// <summary>
    /// Packet type 2;
    /// Sent to the server to update the players cursor position
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 1)]
    [System.Serializable]
    public class Pkt_Update : Packet
    {
        [FieldOffset(5)] public short x;
        [FieldOffset(7)] public short y;

        public Pkt_Update()
        {
            packetType = (byte)packetTypes.UPDATE;
        }
        public override byte[] ToSend()
        {
            byte[] temp = new byte[10];
            BitConverter.GetBytes(packetType).CopyTo(temp, 0);
            BitConverter.GetBytes(packetNum).CopyTo(temp, 1);
            BitConverter.GetBytes(x).CopyTo(temp, 5);
            BitConverter.GetBytes(y).CopyTo(temp, 6);
            return temp;
        }
    }

    /// <summary>
    /// Packet type 3;
    /// Sent to the clients to tell them to stop sending data
    /// </summary>
    //[StructLayout(LayoutKind.Explicit, Size = 1)]
    [System.Serializable]
    public class Pkt_Shutdown : Packet
    {
        public byte[] msg = new byte[3] { (byte)'B', (byte)'y', (byte)'e' };

        public Pkt_Shutdown()
        {
            packetType = (byte)packetTypes.SHUTDOWN;
        }
        public override byte[] ToSend()
        {
            byte[] temp = new byte[10];
            BitConverter.GetBytes(packetType).CopyTo(temp, 0);
            BitConverter.GetBytes(packetNum).CopyTo(temp, 1);
            msg.CopyTo(temp, 5);
            temp[5] = (byte)'B';
            temp[6] = (byte)'y';
            temp[7] = (byte)'e';
            return temp;
        }
    }

    /// <summary>
    /// Packet type 4;
    /// Sent to the client instead of Pkt_Ack if the server is full
    /// </summary>
    [StructLayout(LayoutKind.Explicit, Size = 1)]
    [System.Serializable]
    public class Pkt_Error_ServerFull : Packet
    {
        [FieldOffset(5)] public byte msg = 2;

        public Pkt_Error_ServerFull()
        {
            packetType = (byte)packetTypes.SERVERFULL;
        }
        public override byte[] ToSend()
        {
            byte[] temp = new byte[10];
            BitConverter.GetBytes(packetType).CopyTo(temp, 0);
            BitConverter.GetBytes(packetNum).CopyTo(temp, 1);
            BitConverter.GetBytes(msg).CopyTo(temp, 5);
            return temp;
        }
    }
}
