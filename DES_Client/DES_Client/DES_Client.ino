#pragma pack(push,1)
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

const char* ssid = "Fast2";
const char* password = "Rumboligan12229";

byte clientNum = 0; // 0:Red, 1:Green, 2:Blue, 3:Cyan/Magenta

int xPotPin = 5; //D1
int yPotPin = 4; //D2
short xPotVal = 0;
short yPotVal = 0;

int ledPin = 2; //D4

WiFiUDP udp;
unsigned int port = 1379;
char incomingPacket[255];
IPAddress ipAddress(255, 255, 255, 255);

int serverConnected = 0;
int _packetNum = 0;

class Packet
{
  public:
    byte packetType;
    int packetNum;

    enum packetTypes
    {
      ANNOUNCE,
      ACKNOWLEDGE,
      UPDATE,
      SHUTDOWN,
      SERVERFULL
    };
};

class Pkt_Announce : public Packet
{
  public:
    byte msg[5] = { 'H', 'e', 'l', 'l', 'o' };
    byte colour = clientNum;
    short xStart;
    short yStart;


    Pkt_Announce()
    {
      packetType = packetTypes::ANNOUNCE;
      packetNum = _packetNum;
    }
};

class Pkt_Ack : public Packet
{
  public:
    byte r;
    byte g;
    byte b;
    short axisMaxVal;

    Pkt_Ack()
    {
      packetType = packetTypes::ACKNOWLEDGE;
      packetNum = _packetNum;
    }
};

class Pkt_Update : public Packet
{
  public:
    short x;
    short y;

    Pkt_Update()
    {
      packetType = packetTypes::UPDATE;
      packetNum = _packetNum;
    }
};

template<typename T>
int SendPacket(T toSend)
{
  char* message = (char*)&toSend;
  Serial.print("Packet type: ");
  Serial.print(toSend.packetType);
  Serial.print(" Size: ");
  Serial.print(sizeof(toSend));
  Serial.print(" Packet num: ");
  Serial.println(toSend.packetNum);
  udp.beginPacket(ipAddress, port);
  udp.write(message, sizeof(toSend));
  udp.endPacket();
  Serial.println("Sent");
  _packetNum++;
}

void ParsePacket()
{
  int waiting;
  do
  {
    waiting = udp.parsePacket();
    if (waiting > 0)
    {
      char buffer[10000];
      udp.read(buffer, 255);
      Packet* p;
      p = (Packet*)buffer;
      switch (p->packetType)
      {
        case Packet::ACKNOWLEDGE:
          {
            Serial.println("Connected");
            serverConnected = 1;
            break;
          }
        case Packet::SHUTDOWN:
          {
            serverConnected = 0;
            Serial.println("Server has shutdown.");
            break;
          }
        case Packet::SERVERFULL:
          {
            Serial.println("Error: Server full.");
            serverConnected = 2;
            break;
          }
        default:
          {
            Serial.println("Packet not recognised");
            Serial.println(p->packetType);
            break;
          }
      }
    }
  } while (waiting);
}

short ReadX()
{
  digitalWrite(xPotPin, HIGH);
  short tempX = analogRead(A0);
  digitalWrite(xPotPin, LOW);
  return tempX;
};

short ReadY()
{
  digitalWrite(yPotPin, HIGH);
  short tempY = analogRead(A0);
  digitalWrite(yPotPin, LOW);
  return tempY;
};

void AttemptConnect()
{
  if (_packetNum >= 5)
  {
    Serial.println("Server failed to respond after 5 attempts. Retrying in 10 seconds");
    delay(10000);
  }
  Pkt_Announce announcePkt;
  announcePkt.xStart = ReadX();
  announcePkt.yStart = ReadY();
  SendPacket(announcePkt);
  ParsePacket();
}

void AttemptReconnect()
{
  digitalWrite(ledPin, LOW);
  delay(1000);
  digitalWrite(ledPin, HIGH);
  Serial.println("Attempting server reconnect");
  _packetNum = 0;
  AttemptConnect();
}

void setup()
{
  Serial.begin(9600);

  pinMode(A0, INPUT);
  pinMode(xPotPin, OUTPUT);
  pinMode(yPotPin, OUTPUT);
  pinMode(ledPin, OUTPUT);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  digitalWrite(ledPin, HIGH);
  Serial.println("WiFi connected");
  // Print the IP address
  Serial.println(WiFi.localIP());
  Serial.println(port);
  udp.begin(port);

  Serial.println("Trying to connect to server...");
  while (serverConnected == 0)
  {
    digitalWrite(ledPin, LOW);
    AttemptConnect();
    delay(1000);
    digitalWrite(ledPin, HIGH);
    delay(1000);
  }
  digitalWrite(ledPin, HIGH);
}

void loop()
{
  if (serverConnected == 1)
  {
    xPotVal = ReadX();
    Serial.print("x: ");
    Serial.println(xPotVal);

    delay(50); //Give the pins time to switch

    yPotVal = ReadY();
    Serial.print("y: ");
    Serial.println(yPotVal);

    Pkt_Update updatePkt;
    updatePkt.x = xPotVal;
    updatePkt.y = yPotVal;
    SendPacket(updatePkt);
    ParsePacket();

//    delay(50); //Give the pins time to switch and dont spam the server too much
  }
  else if (serverConnected == 0)
  {
    AttemptReconnect();
  }
  else if (serverConnected == 2)
  {
    AttemptReconnect();
  }
}
